package com.microsoft.gittf.client.clc.commands;

import com.microsoft.gittf.client.clc.ExitCode;
import com.microsoft.gittf.client.clc.Messages;
import com.microsoft.gittf.client.clc.arguments.Argument;
import com.microsoft.gittf.client.clc.commands.framework.Command;
import com.microsoft.gittf.client.clc.commands.framework.CommandTaskExecutor;
import com.microsoft.gittf.core.tasks.CleanupTask;
import com.microsoft.gittf.core.tasks.framework.TaskStatus;

public class CleanupCommand extends Command
{

    public static final String COMMAND_NAME = "cleanup"; //$NON-NLS-1$

    @Override
    protected String getCommandName()
    {
        return COMMAND_NAME;
    }

    @Override
    public Argument[] getPossibleArguments()
    {
        return new Argument[0];
    }

    @Override
    public String getHelpDescription()
    {
        return Messages.getString("CleanupCommand.HelpDescription"); //$NON-NLS-1$
    }

    @Override
    public int run() throws Exception
    {
        verifyGitTfConfigured();

        CleanupTask task = new CleanupTask(getVersionControlClient(), getRepository());
        final CommandTaskExecutor taskExecutor = new CommandTaskExecutor(getProgressMonitor());

        TaskStatus status = taskExecutor.execute(task);
        return status.isOK() ? ExitCode.SUCCESS : ExitCode.FAILURE;
    }
}
