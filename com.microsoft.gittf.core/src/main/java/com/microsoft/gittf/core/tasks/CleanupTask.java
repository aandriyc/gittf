package com.microsoft.gittf.core.tasks;

import com.microsoft.gittf.core.Messages;
import com.microsoft.gittf.core.tasks.framework.Task;
import com.microsoft.gittf.core.tasks.framework.TaskProgressDisplay;
import com.microsoft.gittf.core.tasks.framework.TaskProgressMonitor;
import com.microsoft.gittf.core.tasks.framework.TaskStatus;
import com.microsoft.gittf.core.util.Check;
import com.microsoft.gittf.core.util.DirectoryUtil;
import com.microsoft.tfs.core.clients.versioncontrol.VersionControlClient;
import com.microsoft.tfs.core.clients.versioncontrol.exceptions.ItemNotMappedException;
import com.microsoft.tfs.core.clients.versioncontrol.soapextensions.Workspace;
import org.eclipse.jgit.lib.Repository;

import java.io.File;
import java.io.FileFilter;

public class CleanupTask extends Task
{
    private final VersionControlClient versionControlClient;
    private final Repository repository;

    public CleanupTask(VersionControlClient versionControlClient, Repository repository)
    {
        Check.notNull(versionControlClient, "versionControlClient"); //$NON-NLS-1$
        Check.notNull(repository, "repository"); //$NON-NLS-1$
        this.versionControlClient = versionControlClient;
        this.repository = repository;
    }

    @Override
    public TaskStatus run(TaskProgressMonitor progressMonitor) throws Exception
    {
        progressMonitor.beginTask(Messages.getString("CleanupTask.Start"), //$NON-NLS-1$
                TaskProgressMonitor.INDETERMINATE,
                TaskProgressDisplay.DISPLAY_PROGRESS);
        try
        {
            File baseDir = DirectoryUtil.getGitTFDir(repository);
            File[] children = baseDir.listFiles(new FileFilter()
            {
                public boolean accept(File pathname)
                {
                    return pathname.isDirectory();
                }
            });
            for (File dir : children)
            {
                try
                {
                    Workspace workspace = versionControlClient.getWorkspace(dir.getAbsolutePath());
                    String displayName = workspace.getDisplayName();
                    DeleteWorkspaceTask subTask = new DeleteWorkspaceTask(versionControlClient, workspace, dir);
                    TaskStatus status = subTask.run(progressMonitor.newSubTask(1));
                    progressMonitor.displayMessage(Messages.formatString("CleanupTask.ProgressFormat", displayName, status.isOK() ? "OK" : "FAIL"));
                }
                catch (ItemNotMappedException e)
                {
                    // ignore
                }
            }
            return TaskStatus.OK_STATUS;
        }
        catch (Exception e)
        {
            return new TaskStatus(TaskStatus.ERROR, e);
        }
        finally
        {
            progressMonitor.endTask();
        }
    }
}
