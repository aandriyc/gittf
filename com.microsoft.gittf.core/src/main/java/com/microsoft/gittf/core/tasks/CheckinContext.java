package com.microsoft.gittf.core.tasks;

import java.util.Arrays;
import java.util.Collection;

public class CheckinContext
{
    private int[] queuedBuildIDs;
    private boolean deleteShelvsets;
    private String commitMessage;

    public int[] getQueuedBuildIDs()
    {
        return queuedBuildIDs == null ? null : Arrays.copyOf(queuedBuildIDs, queuedBuildIDs.length);
    }

    public void setQueuedBuildIDs(int[] queuedBuildIDs)
    {
        this.queuedBuildIDs = queuedBuildIDs == null ? null : Arrays.copyOf(queuedBuildIDs, queuedBuildIDs.length);
    }

    public void setQueuedBuildIDs(Collection<Integer> queuedBuildIDs)
    {
        if (queuedBuildIDs == null)
        {
            this.queuedBuildIDs = null;
        }
        else
        {
            int[] tmp = new int[queuedBuildIDs.size()];
            int idx = 0;
            for (Integer i : queuedBuildIDs)
            {
                tmp[idx] = i == null ? -1 : i;
                idx ++;
            }
            this.queuedBuildIDs = tmp;
        }
    }

    public boolean isDeleteShelvsets()
    {
        return deleteShelvsets;
    }

    public void setDeleteShelvsets(boolean deleteShelvsets)
    {
        this.deleteShelvsets = deleteShelvsets;
    }

    public String getCommitMessage()
    {
        return commitMessage;
    }

    public void setCommitMessage(String commitMessage)
    {
        this.commitMessage = commitMessage;
    }
}
