package com.microsoft.gittf.core.tasks;

import com.microsoft.gittf.core.Messages;
import com.microsoft.gittf.core.tasks.framework.NullTaskProgressMonitor;
import com.microsoft.gittf.core.tasks.framework.TaskProgressMonitor;
import com.microsoft.gittf.core.tasks.framework.TaskStatus;
import com.microsoft.gittf.core.util.Check;
import com.microsoft.tfs.core.clients.build.IBuildDetail;
import com.microsoft.tfs.core.clients.build.IBuildServer;
import com.microsoft.tfs.core.clients.build.IQueuedBuild;
import com.microsoft.tfs.core.clients.build.flags.BuildStatus;
import com.microsoft.tfs.core.clients.build.flags.QueryOptions;
import com.microsoft.tfs.core.clients.build.flags.QueueStatus;
import com.microsoft.tfs.core.clients.versioncontrol.VersionControlClient;
import com.microsoft.tfs.core.clients.versioncontrol.soapextensions.Workspace;
import org.eclipse.jgit.lib.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class WaitForBuildsTask extends WorkspaceTask
{

    private final int[] queuedBuildIDs;
    private final boolean deleteShelvesets;

    public final static int CONTAINS_CHECKINS = 1;
    public final static int NO_CHECKINS = 2;

    public WaitForBuildsTask(Repository repository, VersionControlClient versionControlClient, String serverPath, CheckinContext context)
    {
        super(repository, versionControlClient, serverPath);
        Check.notNull(context, "context"); //$NON-NLS-1$
        int[] queuedBuildIDs = context.getQueuedBuildIDs();
        Check.notNull(queuedBuildIDs, "queuedBuildIDs"); //$NON-NLS-1$
        Check.isTrue(queuedBuildIDs.length > 0, "queuedBuildIDs.length > 0"); //$NON-NLS-1$
        this.queuedBuildIDs = queuedBuildIDs;
        this.deleteShelvesets = context.isDeleteShelvsets();
    }

    @Override
    public TaskStatus run(TaskProgressMonitor progressMonitor) throws Exception
    {
        progressMonitor.beginTask(Messages.getString("WaitForBuildsTask.Start"), queuedBuildIDs.length); //$NON-NLS-1$
        try
        {

            GitTFWorkspaceData workspaceData = createWorkspace(new NullTaskProgressMonitor());
            final Workspace workspace = workspaceData.getWorkspace();
            IBuildServer buildServer = workspace.getClient().getConnection().getBuildServer();
            if (buildServer == null)
            {
                // no active build server
                return new TaskStatus(TaskStatus.ERROR, Messages.getString("WaitForBuildsTask.NoServer")); //$NON-NLS-1$
            }

            int completed = 0;
            boolean hasSuccessBuilds = false;
            List<IQueuedBuild> queuedBuilds = new ArrayList<IQueuedBuild>(Arrays.asList(buildServer.getQueuedBuild(queuedBuildIDs, QueryOptions.ALL)));
            for (int i = 0; i < 86400; i++)
            {
                for (Iterator<IQueuedBuild> iterator = queuedBuilds.iterator(); iterator.hasNext(); )
                {
                    String finished = null;
                    IQueuedBuild queuedBuild = iterator.next();
                    IBuildDetail build = queuedBuild.getBuild();
                    boolean deleteShelveset = false;
                    if (queuedBuild == null)
                    {
                        finished = "BUILD NOT FOUND"; //$NON-NLS-1$
                    }
                    else
                    {
                        QueueStatus queuedBuildStatus = queuedBuild.getStatus();
                        if (QueueStatus.COMPLETED.equals(queuedBuildStatus))
                        {
                            BuildStatus buildStatus = build.getStatus();
                            if (BuildStatus.SUCCEEDED.equals(buildStatus))
                            {
                                finished = "SUCCESS"; //$NON-NLS-1$
                                hasSuccessBuilds = true;
                            }
                            else if (BuildStatus.PARTIALLY_SUCCEEDED.equals(buildStatus))
                            {
                                finished = "PARTIAL SUCCESS"; //$NON-NLS-1$
                                hasSuccessBuilds = true;
                                deleteShelveset = true;
                            }
                            else
                            {
                                finished = "FAIL"; //$NON-NLS-1$
                                deleteShelveset = true;
                            }
                        }
                        else if (QueueStatus.CANCELED.equals(queuedBuildStatus))
                        {
                            finished = "CANCEL"; //$NON-NLS-1$
                            deleteShelveset = true;
                        }
                    }
                    if (finished != null)
                    {
                        iterator.remove();
                        completed++;
                        progressMonitor.displayMessage(Messages.formatString("WaitForBuildsTask.StatusFormat", build.getBuildNumber(), finished)); //$NON-NLS-1$
                        if (this.deleteShelvesets && deleteShelveset)
                        {
                            try
                            {
                                workspace.getClient().deleteShelveset(build.getShelvesetName(), build.getRequestedBy());
                            }
                            catch (Exception e)
                            {
                                // ignore
                            }
                        }
                        progressMonitor.setWork(completed);
                    }
                }
                if (completed >= queuedBuildIDs.length)
                {
                    break;
                }
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e)
                {
                    // ignore
                }
                for (IQueuedBuild queuedBuild : queuedBuilds)
                {
                    queuedBuild.refresh(QueryOptions.ALL);
                }
            }

            return new TaskStatus(TaskStatus.OK, hasSuccessBuilds ? CONTAINS_CHECKINS : NO_CHECKINS);
        }
        finally
        {
            disposeWorkspace(new NullTaskProgressMonitor());
        }
    }
}
